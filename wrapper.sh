#!/bin/bash
cd "$(dirname "$0")"
ENV_RUN="RUNENV";
alamat_ip=$(curl ifconfig.me -s);

display_usage() { 
    echo "";
    echo "###################################################################";
    echo "###################################################################";
	echo "###                                                             ###";
    echo "### Akses API :                                                 ###";
    echo "### http://$alamat_ip:5000/                                     ###";
    echo "### http://$alamat_ip:5000/?height=198&weight=91                ###";
    echo "### ganti value height dan weight untuk mengetaui nilai BMI     ###";
    echo "###                                                             ###";
    echo "###                                                             ###";
    echo "### Monitoring API :                                            ###";
    echo "### http://$alamat_ip:5601/app/apm/                             ###";
    echo "###                                                             ###";
    echo "### Logging API :                                               ###";
    echo "### http://$alamat_ip:5601/app/discover                         ###";
    echo "###                                                             ###";
    echo "###                    By Azan Abdul Hakim                      ###";
    echo "###                                                             ###";
    echo "###################################################################";
    echo "###################################################################";
    echo "";
} 

wget -O docker-compose.yaml https://gitlab.com/zantux/bmi-infra/-/raw/main/docker-compose.yaml
sed -i 's/BUILD/'$ENV_RUN'/g' docker-compose.yaml
sudo docker compose pull
sudo docker compose -p sekolahmu up -d --remove-orphans
sudo docker ps -a | grep sekolahmu

display_usage




# bmi-infra



## Local Installation
Please change the branch to "local" and then please download docker-compose.yaml file

### Prerequisites:
- Docker version 20.10.17, build 100c701
- Docker Compose version v2.6.1

### How to run
1. download docker-compose.yaml from "local" branch
2. run in the same directory as docker-compose.yaml : 
```
docker compose -p bmi up -d
```
3. the stack will be run on your local machine
4. here is the address to access :
```
main app : http://localhost:5000
kibana : http://localhost:5601
```
5. use this for login to kibana
```
user : elastic
pass : elastic123
```

## CI/CD Pipeline
I'm using self hosted VPS for storing the service and gitlab for pipeline.
- merge/push to master branch will deploy to prod env
- merge/push to staging branch will deploy to staging env

### How to access :
```
PROD ENV : 
main app   : http://103.171.85.204:5000/?height=167&weight=70
kibana log : http://103.171.85.204:5601/app/discover
kibana apm : http://103.171.85.204:5601/app/apm

STAGE ENV :
main app   : http://103.187.146.143:5000/?height=167&weight=70
kibana log : http://103.187.146.143:5601/app/discover
kibana apm : http://103.187.146.143:5601/app/apm
```
